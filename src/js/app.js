import '../icon.font'
import 'focus-visible'
import {OVERFLOW_HIDDEN, VAR_PADDING_RIGHT, VAR_PADDING_RIGHT_PX} from './constants'
import Swiper from 'swiper/bundle';
import Inputmask from "inputmask";

class App {
  scrollToOffset = 100;

  constructor(config = {}) {
    Object.keys(config).forEach((key) => {
      if (Object.hasOwn(config, key)) {
        this[key] = config[key]
      }
    })

    this.initScrollTo()

    this.body = document.querySelector('body')
    this.header = document.querySelector('header')
    this.initCustom();


    setTimeout(() => {
      // include components here
    }, 0)
  }

  initScrollTo() {
    document.addEventListener('click', (e) => {
      const trigger = e.target.closest('.js-scroll[data-scroll]')
      if (trigger) {
        const target = document.querySelector(trigger.dataset.scroll)
        if (target) {
          const y =
            target.getBoundingClientRect().top +
            window.pageYOffset -
            this.scrollToOffset
          window.scrollTo({top: y, behavior: 'smooth'})
        }
      }
    })
  }

  importAll(r) {
    r.keys().forEach(r)
  }

  setFocusInModal(modal, openBtn, excludeSelector = '') {
    openBtn = openBtn?.[0] || openBtn
    excludeSelector = excludeSelector ? `:not(${excludeSelector})` : ''
    const focusableElements =
      `button${excludeSelector}, [href]${excludeSelector}, input${excludeSelector}, select${excludeSelector}, textarea${excludeSelector}, [tabindex]:not([tabindex="-1"])${excludeSelector}`

    if (!modal) return

    const focusableContent = modal.querySelectorAll(focusableElements)
    if (!focusableContent.length) return

    let firstFocusableElement = focusableContent[0]
    let lastFocusableElement = focusableContent[
    focusableContent.length - 1
      ]

    const listener = (e) => {
      const isTabPressed = e.key === 'Tab' || e.keyCode === 9
      if (!isTabPressed) {
        return
      }

      firstFocusableElement = [...focusableContent].find(el => window.getComputedStyle(el).visibility !== 'hidden') || firstFocusableElement
      lastFocusableElement = [...focusableContent].reverse().find(el => window.getComputedStyle(el).visibility !== 'hidden') || lastFocusableElement

      if (e.shiftKey) {
        // if shift key pressed for shift + tab combination
        if (document.activeElement === firstFocusableElement) {
          lastFocusableElement.focus() // add focus for the last focusable element
          e.preventDefault()
        }
      } else if (document.activeElement === lastFocusableElement) {
        // if tab key is pressed
        // if focused has reached to last focusable element then focus first focusable element after pressing tab
        firstFocusableElement.focus() // add focus for the first focusable element
        e.preventDefault()
      }
    }


    document.addEventListener('keydown', listener, false)

    setTimeout(() => {
      firstFocusableElement.focus()
    }, 500)

    return () => {
      document.removeEventListener('keydown', listener, false)

      if (openBtn) {
        setTimeout(() => {
          openBtn.focus()
        }, 500)
      }
    }
  }

  isElementInViewport(el, horizontalOnly = false) {
    const rect = el.getBoundingClientRect()
    const horizontal = rect.left >= 0 && rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    const vertical = rect.top >= 0 && rect.bottom <= (window.innerHeight || document.documentElement.clientHeight)

    return (
      horizontalOnly ? horizontal : horizontal && vertical
    )
  }

  addOverflowHiddenBody() {
    const padding = this.getScrollbarWidth()
    this.body.style.paddingRight = padding + 'px'
    this.body.style.setProperty(VAR_PADDING_RIGHT, padding / 10 + 'rem')
    this.body.style.setProperty(VAR_PADDING_RIGHT_PX, padding + 'px')
    this.body.classList.add(OVERFLOW_HIDDEN)
  }

  removeOverflowHiddenBody() {
    this.body.classList.remove(OVERFLOW_HIDDEN)
    this.body.style.paddingRight = ''
    this.body.style.removeProperty(VAR_PADDING_RIGHT)
    this.body.style.removeProperty(VAR_PADDING_RIGHT_PX)
  }

  getScrollbarWidth() {
    const documentWidth = document.documentElement.clientWidth
    return Math.abs(window.innerWidth - documentWidth)
  }

  initCustom() {
    Inputmask().mask(document.querySelectorAll("input[type=tel]"));
    Inputmask().mask(document.querySelectorAll("input[type=email]"));

    new Swiper(".js-our-clients-slider", {
      slidesPerView: 1,
      spaceBetween: 30,
      grid: { rows: 3 },
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
      breakpoints: {
        768: {
          slidesPerView: 2,
          grid: { rows: 2 },
        },
        1280: {
          slidesPerView: 4,
          grid: { rows: 2 },
        },
      }
    });

    let forms = document.querySelectorAll('.js-form');
    forms.forEach(function(form, index) {
      form.addEventListener('submit', function(e){
        e.preventDefault();
        e.stopPropagation();
        
        const formData = new FormData(this);

        let isErrorName = formData.get('name') ? false : true;
        let isErrorPhone = (formData.get('phone').length == 18) ? false : true;
        let isErrorEmail = formData.get('mail') ? false : true;
        let isErrorMessage = formData.get('message') ? false : true;
        let isCorrectForm = true; 
        
        if(isErrorName) {
          window.app.addErrorDataFromForm(e.target, 'input[name="name"]');
        } else {
          window.app.removeErrorDataFromForm(e.target, 'input[name="name"]');
          isCorrectForm = false;
        }

        if(isErrorPhone) {
          window.app.addErrorDataFromForm(e.target, 'input[name="phone"]');
        } else {
          window.app.removeErrorDataFromForm(e.target, 'input[name="phone"]');
          isCorrectForm = false;
        }

        if(isErrorMessage) {
          window.app.addErrorDataFromForm(e.target, 'input[name="message"]');
        } else {
          window.app.removeErrorDataFromForm(e.target, 'input[name="message"]');
          isCorrectForm = false;
        }


        if(isCorrectForm) {
          return false;
        }

        // Место под ajax
        window.app.submitHandler(this);

      });
    });

    // закрытие уведомлений от формы
    let btnCloseMessageForm = document.querySelectorAll(".js-form__preloader-btn");
    btnCloseMessageForm.forEach(function(btnClose, index) {
      btnClose.addEventListener('click', function(e) {
        let wrapper = this.closest('.form__preloader');
        
        wrapper.classList.remove('form__preloader_show');
        //wrapper.classList.remove('form__preloader_load');
        wrapper.classList.remove('form__preloader_success');
        wrapper.classList.remove('form__preloader_error');
      });
    });

      // let fileInputs = document.querySelectorAll('input[type="file"]');
      // fileInputs.forEach(function(input, index) {

      //   input.addEventListener('change', function(e) {
      //     const fileName = this.files[0].name;
      //     const fileSize = this.files[0].size;

      //     let wrapper = this.closest('.input-file');
      //     wrapper.querySelector('.input-file__info span').innerHTML  = fileName + "(" + fileSize + ")";
      //     wrapper.querySelector('.input-file__info').classList.add('input-file__info_show');
      //   });

        
      //   let wrapper = input.closest('.input-file');

      //   wrapper.querySelector('.js-input-file__remove').addEventListener('click', function(e) {
      //     console.log("remove file");
      //     let wrapper = this.closest('.input-file');
      //     wrapper.querySelector('input[type="file"]').value = '';
      //     wrapper.querySelector('.input-file__info').classList.remove('input-file__info_show');
      //   });
        
      // });

    let fileInput = document.querySelector('.js-input-file .js-input-file__el');
    const successTypeFile = ['doc', 'docx ', 'pdf'];
    fileInput.addEventListener('change', function(e) {
      let isError = false;
      let errorMessage = '';

      const fileName = this.files[0].name.slice(0, this.files[0].name.lastIndexOf('.'));
      let tmpFileSize = ((this.files[0].size / 1024) / 1024);
      const fileType = this.files[0].name.split('.').at(-1);

      // Проверка на размер файла
      if(tmpFileSize > 10) {
        isError = true;
        errorMessage = 'Превышен максимальный размер файла';
      }

      // Проверка на тип файла
      if(successTypeFile.indexOf(fileType) < 0) {
        isError = true;
        errorMessage = 'Некорректный формат файла';
      }

      
      let blockMessageError = document.querySelector('.js-wrapper-download-file-message');
      let blockMessage = document.querySelector('.js-wrapper-download-file');

      if(isError) {
        blockMessage.classList.remove('wrapper-download-file_show');
        blockMessageError.classList.add('wrapper-download-file_show');
        blockMessageError.querySelector('.js-wrapper-download-file-message-text').innerHTML = errorMessage;

        this.value = '';
        return false;
      } else {
        blockMessageError.classList.remove('wrapper-download-file_show');
      }

      // Если ошибок нет
      const fileSize = tmpFileSize.toFixed(1) + ' MB';
      const text = fileName + ' (' + fileType + ', ' + fileSize + ')';
      
      blockMessage.classList.add('wrapper-download-file_show');
      blockMessage.querySelector('.js-wrapper-download-file-text').innerHTML = text;
    });

    // Очищаем инпут с файлом
    let btnRemoveFile = document.querySelector('.js-wrapper-download-file__remove');
    btnRemoveFile.addEventListener('click', function(e) {
      console.log("remove file");
      fileInput.value = '';
      let blockMessage = document.querySelector('.js-wrapper-download-file');
      blockMessage.classList.remove('wrapper-download-file_show');
      blockMessage.querySelector('.js-wrapper-download-file-text').innerHTML = '';
    });
  }

  addErrorDataFromForm(element, selector) {
    let tagName = element.querySelector( selector );
    let wrapperTagName = tagName.closest( '.input' );
    wrapperTagName.classList.add( 'input_error' );
  }

  removeErrorDataFromForm(element, selector) {
    let tagName = element.querySelector( selector );
    let wrapperTagName = tagName.closest( '.input' );
    wrapperTagName.classList.remove( 'input_error' );
  }

  submitHandler(form) {
    document.querySelector('.js-form__preloader').classList.add('form__preloader_show');
    const formData = new FormData(form);
    let inputs = form.querySelectorAll('input');
    inputs.forEach(function(item, index){
      item.disabled = true;
    });
    
    let request = new XMLHttpRequest();
    request.onreadystatechange = function() { 
      if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
          console.log("SUCCESS");
          document.querySelector('.js-form__preloader').classList.add('form__preloader_success');

          let inputs = form.querySelectorAll('input');
          inputs.forEach(function(item, index){
            item.value = '';
            item.disabled = false;
          });
      } else if (this.readyState === XMLHttpRequest.DONE) {
          console.log("ERROR");
          document.querySelector('.js-form__preloader').classList.add('form__preloader_error');

          let inputs = form.querySelectorAll('input');
          inputs.forEach(function(item, index){
            item.value = '';
            item.disabled = false;
          });
      }
    }
    
    request.open('POST', 'https://api.github.com/repos/symfony/symfony-docs1', true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      
    request.send(formData);
  }
}

document.addEventListener('DOMContentLoaded', () => {
  window.app = new App(appConfig || {})
})
